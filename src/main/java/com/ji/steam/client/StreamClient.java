package com.ji.steam.client;

import com.ji.GreeterStreamGrpc;
import com.ji.HelloReply;
import com.ji.HelloRequest;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.stream.Stream;

/**
 * title:
 * author:吉
 * since:2019/8/22
 */
public class StreamClient {
    public static void main(String[] args) throws InterruptedException {
        ManagedChannel ch = NettyChannelBuilder.forAddress("localhost", 8080)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext().build();
        GreeterStreamGrpc.GreeterStreamStub greeterStreamStub = GreeterStreamGrpc.newStub(ch);
        StreamObserver<HelloRequest> helloRequestStreamObserver = greeterStreamStub.sayHello(new StreamObserver<HelloReply>() {
            @Override
            public void onNext(HelloReply value) {
                System.out.println("client收到|" + value.getMessage());
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {
                System.out.println("onCompleted");
            }
        });
        Stream.of(1, 2, 3, 4, 5).forEach(integer -> {
            helloRequestStreamObserver.onNext(HelloRequest.newBuilder().setName(integer.toString()).build());
        });
        helloRequestStreamObserver.onCompleted();
        Thread.currentThread().join();
    }
}
