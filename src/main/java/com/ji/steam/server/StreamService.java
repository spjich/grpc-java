package com.ji.steam.server;

import com.ji.GreeterStreamGrpc;
import com.ji.HelloReply;
import com.ji.HelloRequest;
import io.grpc.stub.StreamObserver;

/**
 * title:
 * author:吉
 * since:2019/8/22
 */
public class StreamService extends GreeterStreamGrpc.GreeterStreamImplBase {

    @Override
    public StreamObserver<HelloRequest> sayHello(StreamObserver<HelloReply> responseObserver) {
//        Stream.of(1, 2, 3, 4, 5).forEach(integer -> {
//            responseObserver.onNext(HelloReply.newBuilder().setMessage(integer.toString()).build());
//        });
        return new StreamObserver<HelloRequest>() {
            @Override
            public void onNext(HelloRequest value) {
                System.out.println("server 收到|" + value.getName());
                responseObserver.onNext(HelloReply.newBuilder().setMessage(value.getName()).build());
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {
                System.out.println("server onCompleted");
            }
        };
    }
}
