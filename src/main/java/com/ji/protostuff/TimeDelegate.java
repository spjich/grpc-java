package com.ji.protostuff;

import io.protostuff.Input;
import io.protostuff.Output;
import io.protostuff.Pipe;
import io.protostuff.WireFormat;
import io.protostuff.runtime.Delegate;

import java.io.IOException;

public class TimeDelegate implements Delegate<java.sql.Time> {

    public WireFormat.FieldType getFieldType() {
        return WireFormat.FieldType.FIXED64;
    }

    public Class<?> typeClass() {
        return java.sql.Time.class;
    }

    public java.sql.Time readFrom(Input input) throws IOException {
        return new java.sql.Time(input.readFixed64());
    }

    public void writeTo(Output output, int number, java.sql.Time value,
                        boolean repeated) throws IOException {
        output.writeFixed64(number, value.getTime(), repeated);
    }

    public void transfer(Pipe pipe, Input input, Output output, int number,
                         boolean repeated) throws IOException {
        output.writeFixed64(number, input.readFixed64(), repeated);
    }

}