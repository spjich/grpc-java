package com.ji.protostuff;

import com.ji.DemoBean2;
import com.ji.DemoBeanList;
import com.ji.History2;
import com.ji.protostuff.bean.DemoBean;
import com.ji.protostuff.bean.History;
import com.ji.protostuff.bean.ListDemoBean;

import java.util.ArrayList;
import java.util.List;

/**
 * title:
 * author:吉
 * since:2019/8/23
 */
public class PerformanceTest {

    private final static int LIST_SIZE = 1000;
    private final static int HISTORY_SIZE = 1000;

    public static void main(String[] args) {
        ListDemoBean demoBeanList = build();
        byte[] serialize = ProtoBufUtil.serialize(demoBeanList);
        System.out.println(serialize.length);
        DemoBeanList demoBean2List = build2();
        System.out.println(demoBean2List.getSerializedSize());
    }

    private static DemoBeanList build2() {
        DemoBeanList.Builder demoBeanList = DemoBeanList.newBuilder();
        for (int i = 0; i < LIST_SIZE; i++) {
            DemoBean2.Builder demoBean = DemoBean2.newBuilder();
            demoBean.setAge(i);
            demoBean.setBlob("aaabbb"+i);
            demoBean.setName("haha"+i);
            List<History2> l = new ArrayList<>();
            for (int j = 0; j < HISTORY_SIZE; j++) {
                History2.Builder history = History2.newBuilder();
                history.setYear((long) j);
                history.setHistory(String.valueOf(j));
                demoBean.addHistoryList(history);
            }
            demoBeanList.addBeanList(demoBean.build());
        }
        return demoBeanList.build();
    }

    private static ListDemoBean build() {
        ListDemoBean listDemoBean = new ListDemoBean();
        List<DemoBean> demoBeanList = new ArrayList<>();
        for (int i = 0; i < LIST_SIZE; i++) {
            DemoBean demoBean = new DemoBean();
            demoBean.setAge(i);
            demoBean.setBlob("aaabbb"+i);
            demoBean.setName("haha"+i);
            List<History> l = new ArrayList<>();
            for (int j = 0; j < HISTORY_SIZE; j++) {
                History history = new History();
                history.setYear((long) j);
                history.setHistory(String.valueOf(j));
                l.add(history);
            }
            demoBean.setHistoryList(l);
            demoBeanList.add(demoBean);
        }
        listDemoBean.setDemoBeanList(demoBeanList);
        return listDemoBean;
    }
}
