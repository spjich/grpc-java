package com.ji.protostuff;

import io.protostuff.Input;
import io.protostuff.Output;
import io.protostuff.Pipe;
import io.protostuff.WireFormat.FieldType;
import io.protostuff.runtime.Delegate;

import java.io.IOException;

public class TimestampDelegate implements Delegate<java.sql.Timestamp> {

    public FieldType getFieldType() {
        return FieldType.FIXED64;
    }

    public Class<?> typeClass() {
        return java.sql.Timestamp.class;
    }

    public java.sql.Timestamp readFrom(Input input) throws IOException {
        return new java.sql.Timestamp(input.readFixed64());
    }

    public void writeTo(Output output, int number, java.sql.Timestamp value,
                        boolean repeated) throws IOException {
        output.writeFixed64(number, value.getTime(), repeated);
    }

    public void transfer(Pipe pipe, Input input, Output output, int number,
                         boolean repeated) throws IOException {
        output.writeFixed64(number, input.readFixed64(), repeated);
    }

}
