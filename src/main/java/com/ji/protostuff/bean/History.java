package com.ji.protostuff.bean;

/**
 * title:
 * author:吉
 * since:2019/8/23
 */
public class History {
    private String history;
    private Long year;

    public String getHistory() {
        return history;
    }

    public Long getYear() {
        return year;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public void setYear(Long year) {
        this.year = year;
    }
}
