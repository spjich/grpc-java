package com.ji.protostuff.bean;

import java.util.List;

/**
 * title:
 * author:吉
 * since:2019/8/23
 */
public class ListDemoBean {
    private List<DemoBean> demoBeanList;

    public List<DemoBean> getDemoBeanList() {
        return demoBeanList;
    }

    public void setDemoBeanList(List<DemoBean> demoBeanList) {
        this.demoBeanList = demoBeanList;
    }
}
