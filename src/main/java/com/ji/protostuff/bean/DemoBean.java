package com.ji.protostuff.bean;

import java.util.List;

/**
 * title:
 * author:吉
 * since:2019/8/23
 */
public class DemoBean {

    private String name;
    private Integer age;
    private String blob;
    private List<History> historyList;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBlob() {
        return blob;
    }

    public void setBlob(String blob) {
        this.blob = blob;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }
}
