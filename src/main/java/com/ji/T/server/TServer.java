package com.ji.T.server;

import io.grpc.netty.shaded.io.grpc.netty.NettyServerBuilder;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * title:
 * author:吉
 * since:2019/8/23
 */
public class TServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        NettyServerBuilder.forAddress(new InetSocketAddress("localhost", 8080))
                .addService(new TService())
                .build()
                .start();
        Thread.currentThread().join();
    }
}
