package com.ji.T.server;

import com.google.protobuf.Any;
import com.ji.T.DataBody;
import com.ji.T.GreeterTServiceGrpc;
import com.ji.T.HelloReply;
import com.ji.T.HelloRequest;
import io.grpc.stub.StreamObserver;

/**
 * title:
 * author:吉
 * since:2019/8/23
 */
public class TService extends GreeterTServiceGrpc.GreeterTServiceImplBase {

    @Override
    public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        System.out.println("server端收到|" + request.getName());
        DataBody data = DataBody.newBuilder().setName("xixi").setAge(3).build();
        responseObserver.onNext(HelloReply.newBuilder().setType(1).setMessage("xixi")
                .setData(Any.pack(data)).build());
        responseObserver.onCompleted();
    }
}
