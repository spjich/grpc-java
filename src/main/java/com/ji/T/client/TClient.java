package com.ji.T.client;

import com.google.protobuf.InvalidProtocolBufferException;
import com.ji.T.DataBody;
import com.ji.T.GreeterTServiceGrpc;
import com.ji.T.HelloReply;
import com.ji.T.HelloRequest;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;

/**
 * title:
 * author:吉
 * since:2019/8/23
 */
public class TClient {

    public static void main(String[] args) throws InvalidProtocolBufferException, InterruptedException {

        ManagedChannel ch = NettyChannelBuilder.forAddress("localhost", 8080).usePlaintext().build();
        HelloReply reply = GreeterTServiceGrpc.newBlockingStub(ch).sayHello(HelloRequest.newBuilder().setName("xixi").build());
        System.out.println("客户端收到|name=" + reply.getMessage() + "|data=|" + reply.getData().unpack(DataBody.class));
        Thread.currentThread().join();
    }
}
