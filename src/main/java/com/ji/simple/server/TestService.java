package com.ji.simple.server;

import com.ji.GreeterGrpc;
import com.ji.HelloReply;
import com.ji.HelloRequest;
import io.grpc.stub.StreamObserver;

/**
 * title:
 * author:吉辰
 * since:2019/8/22
 */
public class TestService extends GreeterGrpc.GreeterImplBase {
    @Override
    public void sayHello(HelloRequest request, StreamObserver<HelloReply> responseObserver) {
        System.out.println("server 收到|" + request.getName());
        responseObserver.onNext(HelloReply.newBuilder().setMessage("hello xixi").build());
        responseObserver.onCompleted();
    }
}
