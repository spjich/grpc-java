package com.ji.simple.client;

import com.ji.GreeterGrpc;
import com.ji.HelloReply;
import com.ji.HelloRequest;
import io.grpc.ManagedChannel;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;

/**
 * title:
 * author:吉
 * since:2019/8/22
 */
public class Client {
    public static void main(String[] args) throws InterruptedException {
        ManagedChannel ch = NettyChannelBuilder.forAddress("localhost", 8080)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext().build();
        GreeterGrpc.GreeterBlockingStub greeterBlockingStub = GreeterGrpc.newBlockingStub(ch);
        HelloReply xixi = greeterBlockingStub.sayHello(HelloRequest.newBuilder().setName("xixi").build());
        System.out.println("client 收到|" + xixi.getMessage());
        Thread.currentThread().join();
    }
}
